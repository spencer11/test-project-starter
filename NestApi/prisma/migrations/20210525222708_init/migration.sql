-- CreateEnum
CREATE TYPE "PriceRange" AS ENUM ('Cheap', 'Medium', 'Expensive');

-- CreateTable
CREATE TABLE "location" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "distance" INTEGER NOT NULL,
    "price" "PriceRange" NOT NULL DEFAULT E'Cheap',

    PRIMARY KEY ("id")
);
