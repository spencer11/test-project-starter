/*
  Warnings:

  - You are about to drop the `location` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "location";

-- CreateTable
CREATE TABLE "locations" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "distance" INTEGER NOT NULL,
    "price" "PriceRange" NOT NULL DEFAULT E'Cheap',

    PRIMARY KEY ("id")
);
