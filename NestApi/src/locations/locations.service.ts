import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/services/prisma/prisma.service';
import { CreateLocationDto } from './dto/create-location.dto';
import { UpdateLocationDto } from './dto/update-location.dto';

@Injectable()
export class LocationsService {
  constructor(private prisma: PrismaService) {}

  async create(createLocationDto: CreateLocationDto) {
    return await this.prisma.location.create({ data: createLocationDto });
  }

  async findAll() {
    return await this.prisma.location.findMany({ orderBy: { name: 'asc' } });
  }

  async findOne(id: number) {
    return await this.prisma.location.findUnique({ where: { id: id } });
  }

  async update(id: number, updateLocationDto: UpdateLocationDto) {
    return await this.prisma.location.update({
      where: { id: id },
      data: updateLocationDto,
    });
  }

  async remove(id: number) {
    return await this.prisma.location.delete({ where: { id: id } });
  }
}
