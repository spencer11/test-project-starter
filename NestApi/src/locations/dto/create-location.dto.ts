import { PriceRange } from '.prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class CreateLocationDto {
  @ApiProperty()
  name: string;
  @ApiProperty()
  distance: number;
  @ApiProperty({ enum: PriceRange })
  price: PriceRange;
}
