import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LocationsModule } from './locations/locations.module';
import { PrismaService } from './services/prisma/prisma.service';

@Module({
  imports: [LocationsModule],
  controllers: [AppController],
  providers: [AppService, PrismaService],
})
export class AppModule {}
