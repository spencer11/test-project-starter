INSERT INTO "locations" ("id", "name", "distance", "price") VALUES
(1,	'Taco Bell',	2,	'Cheap'),
(2,	'Three Happiness',	5,	'Medium'),
(4,	'Buffalo Wild Wings',	0.25,	'Medium'),
(5,	'Smitty''s Garage',	0.1,	'Medium'),
(3,	'Chicken Place',	7,	'Medium'),
(6,	'Qdoba',	4,	'Medium'),
(7,	'Salsaritas',	3,	'Medium');