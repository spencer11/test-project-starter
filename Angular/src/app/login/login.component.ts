import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  isLoggingIn = false;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  googleLogin() {
    // use this function for handling a google login trigger
    this.router.navigate(['/locations']);
  }
}
