# Frontend Test Project

## The Appsky office needs to compile a list of restaurants that they frequent so that they don't waste so much time deciding where to get lunch.

---

### Technologies Used

- Angular 12
- [Nest.js API](https://nestjs.com/)
- PostgreSQL Database
- Firebase Auth
- Docker [Mac](https://docs.docker.com/docker-for-mac/install/) [Windows](https://docs.docker.com/docker-for-windows/install/)
- MakerFile
  - Mac: `brew install make`
  - Windows: [Unverified but my best guess](http://gnuwin32.sourceforge.net/packages/make.htm)

---

### Setup

Look at the Makefile to familiarize yourself with the commands that are being run during setup.

Watch the video tutorial [here](https://drive.google.com/file/d/1ORCHZSkLxCCk7GXr09fDmDntNGiHJi4Y/view?usp=sharing). You may have to download it to get full video quality.

1. git clone https://gitlab.com/spencer11/test-project-starter.git
2. Open the project
3. (First Terminal) make db

- go to localhost:8080
- system: PostgreSQL
- server: db
- username: postgres
- password: TestProject
- database postgres

4. (Second terminal) make load-dependencies
5. make migrate
6. make api
7. Fill the data in the database using the SQL scrip in `sampleData.sql`
8. Check API docs [here](localhost:3000/docs)
9. (Third terminal) make frontend

---

### Requirements

- Mindset
  - This is YOUR project
  - Any work done up to this point was to give you a head start. Feel free to rearrange or refactor anything and everthing as you see fit.
- GitFlow
  - Create a branch titled your name
  - Push all commits to that branch
  - Create a Merge Request from your branch to master
- Authentication
  - Implement basic google auth using firebase
  - You will be expected to use the [onAuthStateChanged](https://firebase.google.com/docs/auth/web/manage-users) method to monitor the user
  - Users should not be able to go to any page(s) besides the login screen if they are not signed in
- Location Management
  - Create a table for viewing the locations that are stored in the Postgres Database
  - Create a form for adding a new location to the table
  - If there is time, create a button for randomly selecting a restaurant as the destination of the day
