# Start the database environment
db:
	docker-compose up --build -d

# Stop the database environment
stop:
	docker-compose down

# Set up database based on Prisma Schema
migrate:
	npm run -C ./NestApi migrate-db

# Start the database
api:
	npm run -C ./NestApi start

# Start he database and watch for changes
api-dev:
	npm run -C ./NestApi start:dev

# Start angular web app
frontend:
	npm run -C ./Angular start

#
# Commands for running npm install within the frontend and API
#

# Load npm modules for frontend
load-frontend-dependencies:
	npm run -C ./Angular load-dependencies

# Load npm modules for API
load-api-dependencies:
	npm run -C ./Angular load-dependencies

# Load npm modules for whole project
load-dependencies: load-frontend-dependencies load-api-dependencies